<?php

/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 2/22/2017
 * Time: 2:11 PM
 */

namespace GuruAuto;
use InstagramAPI\InstagramException;
use Bugsnag;

include 'vendor/autoload.php';
require_once 'vendor/mgp25/instagram-php/src/InstagramRegistration.php';

class InstaClass {

	public $instance;

	public function __construct($username, $password){

		$bugsnag = Bugsnag\Client::make("36c231c1d628cc10563b3ff982102cab");
		Bugsnag\Handler::register($bugsnag);

		$this->instance = new \InstagramAPI\Instagram(false);

		$this->instance->setUser($username, $password);
		$this->instance->login();

	}

	public function getUsernameID($username){

		$encoded = json_encode($this->instance->searchUsername($username));
		$decoded = json_decode($encoded);

		$id = $decoded->user->pk;

		return $id;
	} // done

	public function getUserFollowers($username){
		$id = $this->getUsernameID($username);

		$response = array();

		$followers = $this->instance->getUserFollowers($id);

		array_push($response,$followers);

		$followers = json_encode($followers);

		$followers = json_decode($followers);


		while(isset($followers->next_max_id) ){

			$followers =  $this->instance->getUserFollowers($id, $followers->next_max_id) ;

			array_push($response,$followers);

			$followers = json_encode( $followers);

			$followers = json_decode($followers);


		}


		return json_encode($response, JSON_UNESCAPED_SLASHES);
	} // done


	public function getUserFollowings($username){
		$id = $this->getUsernameID($username);

		$response = array();

		$followings = $this->instance->getUserFollowings($id);

		array_push($response,$followings);

		$followings = json_encode($followings);

		$followings = json_decode($followings);


		while(isset($followings->next_max_id) ){

			$followings =  $this->instance->getUserFollowings($id, $followings->next_max_id) ;

			array_push($response,$followings);

			$followings = json_encode( $followings);

			$followings = json_decode($followings);


		}


		return json_encode($response, JSON_UNESCAPED_SLASHES);
	} // done

	public function getMediaLikers($url){
		$mediaData = file_get_contents("https://api.instagram.com/oembed/?callback=&url=" . $url);

		$mediaData = json_decode($mediaData);

		$mediaID = $mediaData->media_id;

		$likers = $this->instance->getMediaLikers($mediaID);
		return json_encode($likers);
	} // done


	public function getMediaId($url) {
		$mediaData = file_get_contents("https://api.instagram.com/oembed/?callback=&url=" . $url);

		$mediaData = json_decode($mediaData);

		$mediaID = $mediaData->media_id;

		return $mediaID	;

	} // done
	public function getUserFeed($username){
		return json_encode($this->instance->getUserFeed($this->getUsernameID($username)) );

	} // done

	public function sendMail( $subject , $message, $email) {

		$subject = rawurlencode($subject);

		$message = rawurlencode($message);

		file_get_contents("https://worker.igauto.pro/mail.php?to=$email&subject=$subject&message=$message");

	} // done




	public function likeTagPhotos($tag,$delay,$max){

		$data = json_encode( $this->instance->getHashtagFeed($tag) );
		$data = json_decode($data);

		$count = 0;
		foreach($data->items as $item){

			if($count < $max){
				$this->instance->like($item->id);
				$count++;
				sleep($delay);
			}
		}


		while($data->more_available && ($count < $max) ){

			$data = json_encode( $this->instance->getHashtagFeed($tag,$data->next_max_id) );
			$data = json_decode($data);

			foreach($data->items as $item){
				$this->instance->like($item->id);
				sleep($delay);
				$count++;
			}

		}
		return "Liked $count photos";

	} // done


	public function unfollowNonFollowers($delay){

		$followers = json_decode( $this->getUserFollowers($this->instance->username) ); // self users username id
		$followerKeys = array(); // store username IDs

		// get usernameIds and store
		foreach($followers as $item){
			foreach($item->users as $user){
				array_push($followerKeys, $user->pk);

			}
		}

		$followings = json_decode($this->getUserFollowings( $this->instance->username ));
		$followingKeys = array(); // store username IDs

		// get following usernameIDs and store
		foreach($followings as $item){
			foreach($item->fullResponse->users as $user){
				array_push($followingKeys, $user->pk);

			}
		}


		$count = 0;

		// foreach following, run through all followers, if the person doesn't exist. Un-follow.
		foreach($followingKeys as $followingkey){
			$toUnfollow = true ;

			foreach($followerKeys as $followerkey){

				if($followerkey == $followingkey){ // if the user is following dont unfollow
					$toUnfollow = false;
				}
			}

			if($toUnfollow){
				$this->instance->unfollow($followingkey);
				sleep($delay);
				$count++;
			}

		}

		return "Un-followed $count accounts";

	} // done


	public function followUserFollowers($username, $delay, $dm){

		$followers = json_decode( $this->getUserFollowers($username) );

		$count = 0;
		// get usernameIds and store
		foreach($followers as $item){
			foreach($item->users as $user){

				$this->instance->follow($user->pk);

				if(!empty($dm))
					$this->instance->direct_message( [ $user->pk ], "Just followed you. Please follow back. Thanks :)" );

				$count++;
				sleep($delay);

			}
		}

		return "Followed $count accounts";
	} // done


	public function followMediaLikers($url, $delay,$dm){
		$data = $this->getMediaLikers($url);
		$data = json_decode($data);

		$count = 0;

		foreach($data as $item){
			foreach($item->users as $user){

				$this->instance->follow($user->pk);
				if(!empty($dm))
				$this->instance->direct_message([$user->pk], "Just followed you. Please follow back. Thanks :)");
				sleep($delay);
				$count++;
			}
		}

		return "Followed $count users";
	} // done

	public function likeMediaLikers($url, $delay, $max = 2){
		$data = $this->getMediaLikers($url);
		$data = json_decode($data);

		$countLikes = 0;
		$countUsers = 0;

		foreach($data as $item){
			foreach($item->users as $user){

				try {
					$feed = json_encode( $this->instance->getUserFeed( $user->pk ) );
					$feed = json_decode( $feed );

					for ( $i = 0; $i < $max; $i ++ ) {

						$this->instance->like( $feed->items[ $i ]->id ); // like the last 'max' number of photos from a liker's feed
						$countLikes ++;

					}
					sleep( $delay );
					$countUsers ++;
				} catch(InstagramException $e) {
					error_log($e);
				}
			}
		}

		return "Liked $countLikes media on $countUsers accounts";
	} // done

	public function unlikeMediaLikers($url, $delay, $max = 2){
		$data = $this->getMediaLikers($url);
		$data = json_decode($data);

		$countLikes = 0;
		$countUsers = 0;

		foreach($data as $item){
			foreach($item->users as $user){

				try {
					$feed = json_encode( $this->instance->getUserFeed( $user->pk ) );
					$feed = json_decode( $feed );

					for ( $i = 0; $i < $max; $i ++ ) {

						$this->instance->unlike( $feed->items[ $i ]->id ); // like the last 'max' number of photos from a liker's feed
						$countLikes ++;

					}
					sleep( $delay );
					$countUsers ++;
				} catch(InstagramException $e) {
					error_log($e);
				}
			}
		}

		return "Liked $countLikes media on $countUsers accounts";
	} // done


	//done and tested but unpublished
	public function unfollowAllFollowing($delay){

		$following = json_decode($this->getUserFollowings($this->instance->username));

		$count = 0;
		foreach($following as $item){
			foreach($item->fullResponse->users as $user) {
				$this->instance->unfollow( $user->pk );
				$count ++;
				sleep( $delay );
			}
		}

		return "Unfollowed $count accounts";

	} // done
	public function deleteAllPosts($delay){

		$countLikes = 0;
		try {
			$feed = json_encode( $this->instance->getUserFeed( $this->instance->username_id ) );
			$feed = json_decode( $feed );


			for ( $i = 0; $i < count($feed->items); $i ++ ) {

				$this->instance->deleteMedia( $feed->items[ $i ]->id ); // like the last 'max' number of photos from a liker's feed
				$countLikes ++;

			}
			sleep( $delay );

		} catch(InstagramException $e) {
			error_log($e);
		}

		return "Deleted $countLikes posts";
	} //done
	public function shareToUserFollowers($url, $username, $delay){

		$followers = json_decode( $this->getUserFollowers($username) );

		$count = 0;
		// get usernameIds and store
		foreach($followers as $item){
			foreach($item->users as $user){

				$this->instance->direct_share($this->getMediaId($url),$user->pk);
				$count++;
				sleep($delay);

			}
		}

		return "Followed $count accounts";
	} // done
	public function commentMediaLikers($url, $comment, $delay, $max = 2){
		$data = $this->getMediaLikers($url);
		$data = json_decode($data);

		$countComments = 0;
		$countUsers = 0;

		foreach($data as $item){
			foreach($item->users as $user){

				try {
					$feed = json_encode( $this->instance->getUserFeed( $user->pk ) );
					$feed = json_decode( $feed );

					for ( $i = 0; $i < $max; $i ++ ) {

						$this->instance->comment($feed->items[ $i ]->id, $comment);

						$countComments ++;

					}
					sleep( $delay );
					$countUsers ++;
				} catch(InstagramException $e) {
					error_log($e);
				}
			}
		}

		return "Commented on $countComments media on $countUsers accounts";
	} // done
	public function messageUserFollowers( $username, $message, $delay){

		$followers = json_decode( $this->getUserFollowers($username) );

		$count = 0;
		// get usernameIds and store
		foreach($followers as $item){
			foreach($item->users as $user){

				$this->instance->direct_message($user->pk, $message);
				$count++;
				sleep($delay);

			}
		}

		return "Sent $count messages";
	} // done




	// dont but untested

	public function likeTimelinePhotos($delay){

		$timeline = $this->instance->timelineFeed()->feed_items;

		$jsonTimeline = json_encode($timeline,JSON_UNESCAPED_SLASHES);

		$jsonTimeline = json_decode($jsonTimeline);

		$count = 0;
		foreach($jsonTimeline as $item){

			$id = $item->media_or_ad->id;

			$this->instance->like($id);
			sleep($delay);

			$count++;
		}

		echo "Liked $count photos";
	} // done

}